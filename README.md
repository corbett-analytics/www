# [sfa.com](https://tjwaterman99.gitlab.io/sfa/)

A [hugo](https://gohugo.io/documentation/) based static site.

### Development set up

#### [Docker]

Clone the project.

```
git clone https://gitlab.com/tjwaterman99/sfa.git \
  && cd sfa
```

Build the Dockerfile. It will be tagged as `sfa-hugo`.

```
bin/build
```

Run the Docker image. This will publish the right ports and mount the current working directory (should be the sfa repo).

```
bin/run
```

The server is available at [`localhost:1313`](https://www.localhost:1313).

```
open https://www.localhost:1313
```

You can also start a shell.

```
bin/sh
```

#### [MacOS]

Install hugo.

```
brew install hugo
```

Clone the project.

```
git clone https://gitlab.com/tjwaterman99/sfa.git
```

Start the webserver.

```
hugo serve
```

The site is available at https://127.0.0.1:1313.

### Publishing

Pushes to the master branch on gitlab are built and published automatically.
