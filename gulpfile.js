var gulp = require("gulp")
var sass = require("gulp-sass")
var hash = require("gulp-hash")
var del = require("del")

// Compile SCSS files to CSS
gulp.task("scss", function () {

    // Clean files
    del(["static/css/**/*"])

    gulp.src(["src/scss/**/*.scss", "node_modules/bootstrap/scss/bootstrap.scss"])
        .pipe(sass({
            outputStyle : "compressed"
        }))
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(hash())
        .pipe(gulp.dest("static/css"))
        //Create a hash map
        .pipe(hash.manifest("hash.json"))
        //Put the map in the data directory
        .pipe(gulp.dest("data/css"))
})

// Hash images
gulp.task("images", function () {

    // Clean files
    del(["static/images/**/*"])
    gulp.src("src/images/**/*")
        .pipe(hash())
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(gulp.dest("static/images"))
        .pipe(hash.manifest("hash.json"))
        .pipe(gulp.dest("data/images"))
})

// Hash javascript
gulp.task("js", function () {

    // Clean files
    del(["static/js/**/*"])

    gulp.src("src/js/**/*.js")
        .pipe(hash())
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(gulp.dest("static/js"))
        .pipe(hash.manifest("hash.json"))
        .pipe(gulp.dest("data/js"))
})

// Watch asset folder for changes
gulp.task("watch", ["scss", "images", "js"], function () {
    gulp.watch("src/scss/**/*", ["scss"])
    gulp.watch("src/images/**/*", ["images"])
    gulp.watch("src/js/**/*", ["js"])
})

// Set watch as default task
gulp.task("default", ["scss", "js", "images"])
