Hello {{ FirstName }},

We're thrilled you're taking our course.

Feel free to hit "reply" and ask us a question at any time. I can't always respond, but I do take the time to read every comment. Nothing piece of feedback is too small.

I'm also curious, what's the main reason you chose to take this course today? I'm always looking to make the content here better and more relevant, so if you can take a second to hit "reply" I'd love to know.

Over the next week I'll be sending 4 more emails, each with a different topic relevant for increasing the impact of your analytics.

1. Choosing the right metrics: how successful teams are empirical about their impact
2. Best practices of analytics tools: everything you need to empower your team
3. Hiring a team: what you need to know about structuring and hiring to be as impactful as possible
4. More resources: a collection of my favorite writing and topics on the industry for you to go deeper

Right now I want to share one lesson from the training I took in my first week at Facebook as a data scientist. It's about how to best work with non-technical or non-analytics people.

It boils down to the fact that even if you have all of the right metrics, tools, and skills to be a great analytics team, ultimately your impact is determined by how effectively you'll work with others.

So the lesson is just an analogy. If you've ever gone to an airport you know about the bomb-sniffer dog teams. They're a team of a dog that's trained to smell chemicals commonly used in explosives and a handler to direct them.

Non-technical people are like the bomb-sniffer, and analytics is like the sniffer's handler. What's important to realize is the dog has skills the handler doesn't, and the handler has skills the dog doesn't. But together they're able to accomplish something that neither could possibly accomplish by themselves.

It seems obvious that there are many examples of these kinds of teams, but in my work I've noticed that we all have a bias to believe our own skills are the most relevant and most valuable in every situation. It will significantly impact your career to become aware of that bias and to try and counteract it by remembering the bomb-sniffer dog.

So thanks again for signing up for this course. Always feel free to hit "reply" and let me know what you think!

I've scheduled the next part of the series for tomorrow morning, so look out for it then.

Take care,
Tom

PS if you'd ever like to recommend or share the course with a friend, you can just forward them this email. I'll include links to subscribe to the course at the bottom
