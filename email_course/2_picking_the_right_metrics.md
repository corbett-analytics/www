Hi {{ FirstName }},

This is part 1 of the 4 part series on technology analytics from Corbett Analytics.

1. [This email] Choosing the right metrics: how successful teams are empirical about their impact
2. Best practices of analytics tools: everything you need to empower your team
3. Hiring a team: what you need to know about structuring and hiring to be as impactful as possible
4. More resources: a collection of my favorite writing and topics on the industry for you to go deeper

Picking the right metric is maybe the key foundational skill of data science. In fact at every tech company I've ever interviewed with, I'll get asked some variant of this question:

__How would you measure [X] ?__

For example: How would you measure the reliability of our service? How would you quantify the performance of a show licensed by Netflix? How would you determine if a facebook page is owned by a small, local business? (I actually got asked essentially those questions at my interviews with Netflix, Facebook, and Snowflake!) Even though the question can vary a lot, they're all assessing the same skill.

But it's important to appreciate that those questions aren't just asked in interviews, in the way that algorithm questions are asked in software engineering interviews but never really come up on the job. _You'll be asking yourself the same types of questions on every data science project you tackle_.

Here's maybe the most common question about selecting a metric that comes up: "How do I measure whether my early stage company is having traction?"

You can find a long explanation from Y Combinator

Let's use the example of selecting a "reliability" metric for a service like gmail.

In a sentence, the best approach is to choose the most obvious metric that comes to mind, ask the following questions about it, and then adapt the metric if necessary.

So the most immediate metric that comes to mind with respect to reliability is "uptime." You can quantify that by saying "the percent of minutes in a day that the service is running."

**What is the metric being used for?**

Suppose that the metric will be displayed on a services status page. This is very common: companies will frequently proudly show lots of green lines on their pages to display how trustworthy and reliable they are. [Here's what Amazon Web Services shows.](https://status.aws.amazon.com/)

**Do changes in the metric affect people differently or the same?**

For example, suppose you have customers in different parts of the world, and the service is down for some countries but not others. Clearly, our reliability metric of "uptime" doesn't account for the fact that

**Can you "game" the metric by improving it without creating value?**

"All metrics are wrong, but some are useful".

[Startup school (YC) picking a metric](https://www.startupschool.org/videos/64)

So that's it for Part 1 of this course. We'll be sending out Part 2 (Analytics tools) in the next couple of days.

As always, if you have any feedback about the content, feel free to hit "reply" and let me know. I do read every email I receive.

And if you found this course helpful, please do forward it to a colleague.

Thanks again for following along!

Tom
Founder, Corbett Analytics
https://corbettanalytics.com/
https://twitter.com/tjwaterman99
