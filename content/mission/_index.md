---
title: Our mission
description: "Drive more impact from your organization's analytics. Built by data scientists from Facebook, growth stage startups, and consulting firms in San Francisco"
published: true
---

We're focused on one thing: helping increase the impact of your analytics.

Having spent many years working in analytics at every type of company, including consulting firms, growth stage startups, and large technology companies, we have found that effective analytics organizations practice the same set of values.

First, they believe in the value created by using technology as effectively as possible. Often that means using code, but not always. It also means experimenting with new tools when they're available, and having a vision for how to use them most effectively.

Second, they practice communication. Groups of people are always able to accomplish more together, but only if they communicate effectively. Teams where a single strong contributor pulls the majority of the weight are always performing below their capacity. Effective leaders and teams know this, and they take the steps necessary to create an environment and a system where anyone with a good attitude and energy can contribute impact.

Third, they're empirical when they need to be, but they don't pretend every decision comes down to the numbers. The critical mistake we've seen time and again is resistance to using facts when they should be used, and trying to prove a point with facts that can't be made. We know that there are times when the historical records are critical, and times when they should be discounted. Teams become challenged when they can't separate their personal interests from the right approach.

Taken together, the values of technology, communication, and empiricism, are how we approach helping you increase the impact of your analytics. You can see this in how we structure the content we've built: processes are about communication, analyses are about empiricism, and tools are about technology. We hope that you'll find our resources impactful for your own organization, company, team, or role.
