---
title: "Atom"
description: This tutorial shows how to mount remote directories on your local filesystem so that you can edit them with Atom, Sublime, Finder, or any other tool
date: 2019-06-30T12:00:00Z
image: atom-logo.png
list: false
---

Install atom, sshfs, osxfuse, connect to remote server


## Installation

Install sshfs.

```
brew install sshfs
```

Add the EC2 instance's public DNS to your ssh config.

```
nano ~/.ssh/config
```

- Use sshfs to mount a remote directory

## Install nginx


## Edit a file on the remote server
Remove the default html pages that Nginx provides.

```
sudo rm -rf /var/www/html
```

Make a new folder in your user's home directory.

```
mkdir ~/html
```

Link the directories.

```
sudo ln -s ~/html /var/www/html
```

Now exit the remote server.

```
exit
```

Mount the remote filesystem.

```
sshfs nginx:/home/ubuntu /nginx
```

You'll now be able to navigate the server's filesystem in finder, and edit files on the system using any text editor (Atom, VSCode, Sublime, etc).
