---
title: "EC2 Servers"
date: 2019-06-30T12:00:00Z
frontpage: false
list: false
displayInList: false
dropCap: false
---

One of the most useful skills to acquire as a data scientist is the ability to use remote servers effectively. This tutorial will explain how to create a server using Amazon Web Services, commonly called EC2 instances.

## Create an instance

Go to the AWS console and hit next on everything.

- Ensure that the instance has a security group which allows SSH access.
- Update your ssh config

## Create a new user

```
sudo adduser tom
```

Add your user to the super users group.

```
sudo usermod -aG sudo tom
```

Switch to your new user.

```
su tom
```

## Productivity tools

Since you'll be using the terminal a lot, it's worth investing in some productivity tools.

- Add the public key installed by AWS to your `authorized_keys` file
- Install zsh, a more feature-rich shell than bash
- Update your local machine's `~/.ssh/config` file to use the new user
