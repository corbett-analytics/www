---
title: "Awless"
date: 2019-06-30T12:00:00Z
list: false
displayInList: true
dropCap: false
---

[Awless](https://github.com/wallix/awless) is a CLI for managing AWS resources that's simpler and easier to use than the `awscli`. It provides features like connecting to an instance by name and visualizations of active AWS resources, among others. If you're familiar with the Heroku or Digital Ocean CLIs, Awless provides a similarly high level of abstraction for managing your AWS accounts.

This tutorial will show how to use Awless to provision an EC2 instance and connect to it over `ssh`. It assumes that you've already created an AWS account and that you're using Mac OSX. If you're using linux the steps are the same except for installing Awless: follow the instructions [here](https://github.com/wallix/awless#install) for more details.

## Installation

To get started, install Awless with brew.

```
brew install awless
```

You can confirm Awless is installed correctly by asking for its version.

```
awless version
```

You'll see some text art and some numbers below it. This tutorial uses `version=v0.1.11`.

## Account configuration

Before using awless to manage any AWS resources, you'll need to configure AWS. For this tutorial we'll create a new AWS user, give that user programatic access to the account, and store the user's credentials on the machine where we've installed awless.

To create a new user, log into the AWS account you're using at [aws.amazon.com](https://aws.amazon.com/console). Then use [this link](https://console.aws.amazon.com/iam/home?region=us-west-2#/users$new?step=details) to open the relevant AWS service (IAM) and follow the steps to create a new user.

When filling out the forms, use the following configuration:

- Page 1. Name the user `awless`, and check the box for `Programatic access` under the "Access type" section
- Page 2. Add the user to the default `Admin` group. Although full admin access is generally not advised for production systems, for the purpose of this tutorial it's fine
- Page 3. Don't select any tags
- Page 4. Select confirm to create the new user

On page 5, after confirming the new user creation, you'll see an option to "Download .csv" with the user's credentials. Go ahead and download them.

> Warning: don't close the window yet - you'll need the `Access key ID` and `Secret access key` available from that page in the next step.

To provide the `Access key ID` and `Secret access key` to Awless, use the `whoami` command.

```
awless whoami
```

You'll be prompted for the `Access key ID` and `Secret access key` that are available on the last page of the AWS user creation flow. You'll also be asked whether the to change the profile name or keep `default` - using default is fine, and we assume you've selected `default` for the rest of this tutorial.

After entering the information above, you'll be shown the various access policies which Awless is using. The last line should show that Awless has administrator access.

```text
Policies from group 'Admins': AdministratorAccess
```

To test that everything is working, try listing the EC2 instances on your account.

```bash
awless list instances
```

You might see none listed if your account has no active instances, which is okay.

## Create an EC2 instance

Creating an EC2 instance with Awless also requires the following AWS resources to be configured:

1. Keypair:
2. Virtual private cloud (VPC):
3. Subnet on the VPC:
4. Internet gateway
5. Security group:
6. EC2 instance:

We walk through creating each of these resources in the following sections.

### 1. Keypair

```bash
awless create keypair name=awless
```

You can confirm that your keypair has been created and is registered with your AWS account.

```bash
awless list keypairs
```

By default Awless will also store the private key in the `~/.awless/keys` directory, and you should confirm the private key is accessible there as well.

```
ls ~/.awless/keys
```

### 2. Virtual private cloud

Create a VPC with the right policies...

```bash
awless create vpc \
  name=awless-vpc \
  cidr=10.0.0.0/24
```

### 3. Subnet

Create a subnet with the right policies...

```bash
awless create subnet \
  name=awless-subnet-1 \
  cidr=10.0.0.0/24 \
  public=true \
  vpc=@awless-vpc
```

### 4. Internet gateway

Create an internet gateway...

```
awless create internetgateway
```

Find the id of the gateway you just created. Unfortunately, Awless does not support referring to internet gateways by name.

```
awless list internetgateways
```

Attach the internet gateway to your VPC.

```
awless attach internetgateway vpc=@awless-vpc id={{ INTERNET_GATEWAY_ID }}
```

Update the route table of the subnet.

```
awless create route cidr=0.0.0.0/0 gateway={{ INTERNET_GATEWAY_ID }} routetable=
```

### 5. Security group

Get your IP address

```
export MY_IP_ADDRESS=$(curl https://ifconfig.co/)
```

Create a new security group.

```bash
awless create securitygroup \
  name=ssh \
  vpc=@awless-vpc \
  description="ssh-connections"
```

Update the security group to allow SSH connections.

```bash
awless update securitygroup \
  id=@ssh \
  inbound=authorize \
  protocol=tcp \
  cidr=0.0.0.0/0 \
  portrange=22
```

### 6. EC2 instance

Create the instance.

```bash
awless create instance \
  name=awless-tutorial \
  keypair=awless \
  type=t2.micro \
  securitygroup=@ssh subnet=@awless-subnet-1   
```

Your instance will take a few minutes to finish starting up. To check the status of the instance:

```
awless list instances
```

Once started, you can connect to the instance with Awless using just the instance's name.

```
awless ssh awless-tutorial
```

## Clean up resources

- Snapshot the web server and shut down the instance
