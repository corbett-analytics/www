---
title: "Installing Apache Airflow on Ubuntu 18.04"
date: 2019-06-30T12:00:00Z
frontpage: true
list: true
displayInList: false
dropCap: false
description: "Learn how to get Airflow up and running on an AWS Ubuntu server"
h1: "Installing Apache Airflow on Ubuntu 18.04"
keywords:
  - Airflow
  - Install airflow
  - Install airflow ubuntu
  - Install airflow ec2
---

Airflow has become one of the most popular and must-have tools for analytics organizations today. With Airflow, data scientists can write data pipelines as simple Python modules, integrate them with virtually all commonly used third party systems, and manage them through a secure web interface.

Originally started by Airbnb as an open source project, it was [accepted as an Apache "incubator" project](https://incubator.apache.org/projects/airflow.html) in early 2016 and graduated in late 2018. With a highly active development community, Airflow is likely the best choice for building data pipelines available today.

This tutorial walks through installing Airflow on a Ubuntu 18.04 server running on AWS EC2. It assumes you can access the web server with a shell, and that you'll also be able to edit the configuration of the server through the AWS console. It also expects that you're generally familiar with the concepts behind Airflow, especially DAGs, but [the project's quickstart tutorial](https://airflow.apache.org/tutorial.html) provides a good starting point if you're just getting started.

## Install Airflow and Postgres

For this tutorial we'll use Python 3 and a Postgres database for Airflow's metadata. The metadata store is necessary for Airflow to determine which DAGs have been ran, whether one of the individual tasks of a DAG is eligible to start running, and it also allows you to securely store connection credentials necessary to interface with other systems like S3 or Salesforce.

```
sudo apt-get update && \
sudo apt-get install -y \
  python3-pip \
  postgresql \
  postgresql-contrib
```

We're now ready to install Airflow. Note that the package's name is actually `apache-airflow` and that we're including the optional postgres dependencies by appending `[postgres]` to the package name.

```
pip3 install apache-airflow[postgres]==1.10.3 flask==1.0.4
```

{{% alert warning "At the time this article was written Airflow's dependencies didn't specify the version of Flask to use, and the recently released Flask v1.1 broke the Airflow webserver. We've specified an earlier flask version with `flask==1.0.4` to avoid the breaking change until Airflow's authors can release an update" %}}

On Ubuntu18 pip will install the Airflow executable in `/home/ubuntu/.local/bin`, but by default that directory is not searched when running commands. To fix this, add that directory's name to the `$PATH` variable.

```
export PATH=$PATH:$HOME/.local/bin
```

If you'd like the `$PATH` variable to automatically be set again the next time you login, run the following command to update your `~/.bashrc` file.

```
echo 'export PATH=$PATH:$HOME/.local/bin' >> ~/.bashrc
```

At this point you should be able to access the airflow CLI, but most of its commands will be broken because it does not yet have its metadata database configured. To fix this, we'll need edit Airflow's configuration file.

By default, Airflow will look for its configuration in `~/airflow/airflow.cfg`. It can also read environment variables, but we'll just update the config so that the environment does not have to be reset the next time you log back into the server.

Create that file now.

```
mkdir ~/airflow && touch ~/airflow/airflow.cfg
```

We need to make two changes to the `~/airflow/airflow.cfg` file in order for Airflow to work correctly.

1. First, we need to tell Airflow how to access its metadata database, which we do by setting the `sql_alchemy_conn` value. That will point to the local postgres installation we just created.
2. Second, we need to tell Airflow to authenticate users before allowing them to access the Airflow webserver, which we do by setting the `authenticate` value. Airflow supports many different authentication schemes, but the default requires just a username and password, which is good enough for this tutorial. In a production system you would certainly want to only allows HTTPS connection to the app and you'd also likely use a different authentication scheme.

To make these changes, paste the following text into the `~/airflow.cfg` file.

```make
[core]
# URL of the Airflow metadata db. We're using a local install
# of Postgres
sql_alchemy_conn = postgresql://airflow:password@localhost:5432/airflow

[webserver]
# Require the webserver to authenticate users with an email
# and password
authenticate = True
rbac = True
```

The `sql_alchemy_conn` value we supplied in the `airflow.cfg` file tells Airflow to login to the Postgres server as a user named `airflow` with a password `password` and to store its data in the database named `airflow`. Since that database and user do not exist, we need to create them now.

When you installed Postgres it also installed commands for creating new users and databases which we will run now. But for those commands to work, they need to be ran from the `postgres` user: we'll use `sudo -u postgres bash -c "..."` to run the `createdb` command as if we were logged into the server as the `postgres` user.

```
sudo -u postgres bash -c "createdb airflow"
sudo -u postgres bash -c "createuser airflow --pwprompt"
```

{{% alert warning "the `createuser` command will prompt you for a password for the `airflow` user. Make sure to use the password `password` so that the configuration in `~/airflow/airflow.cfg` will function properly." %}}

Airflow is now ready to initialize its database.

```
airflow initdb
```

You'll see Airflow log some information about the migrations that it's running on the database. To see the list of tables that Airflow created, use the following command.

```
sudo -u postgres bash -c "psql -d airflow -c '\dt'"
```

At this point the Airflow installation is working correctly and we're ready to start the Airflow services.

## Run the airflow services

For our installation, we'll use two of Airflow services: a scheduler and a webserver.

- **The scheduler** is responsible for actually running the Airflow DAGs. The scheduler will query the database to see which DAGs should be ran, execute their logic, and then log the final status of the DAG back to the database. (Note that on Airflow setups which use multiple nodes, the scheduler is actually not responsible for running the DAGs, but for our single-node setup it will be).
- **The webserver** is responsible for creating the DAG visualizations and "front end" that you can access from the web browser. If your DAGs are not running, it is likely an issue with the scheduler and not the webserver.

Start the Airflow services now. We'll add the `--daemon` flag to run the processes as daemons, so they'll continue running even after you log off the webserver.

```
airflow webserver --daemon &&
airflow scheduler --daemon
```

But before you can access the webserver's UI, you'll need to configure the security groups of your EC2 instance to allow traffic over port `8080`.

## Configure the EC2 instance

Update the security groups on your instance to allow HTTP traffic on port 8080, then navigate back to your instance's public DNS address.

You'll be greeted with a login page asking for a user name and password. Before you can log in, you'll need to create an Airflow user.

## Create an Airflow user

We'll use the airflow `create_user` command to create a user that can log into the airflow webserver.

```
airflow create_user \
  --username admin \
  --role Admin \
  --email airflow@mydomain.com \
  --firstname airflow \
  --lastname admin \
  --password {{ SET-PASSWORD-HERE }}
```

Now you can log in with the username and password just configured.

## Run an example DAG

After logging into the Airflow webserver, you'll see a list of example DAGs that Airflow includes by default. The DAGs can be turned on and off through the web interface: try turning some of them on now.

You may need to refresh the page to start seeing the DAGs and their individual tasks start running. If your scheduler is running correctly, you'll start to see the DAGs execute.

It's also possible to turn the DAGs on and off using the Airflow CLI.

Turn on the `example_bash_operator` DAG with the command line.

```
airflow unpause example_bash_operator
```

However you choose to start the DAGs, you should also be able to see that Airflow has begun logging information about the state of each task in files located at `~/airflow/logs/...`.

```
ls ~/airflow/logs/example_bash_operator
```

## Next steps

In this tutorial we set up an Airflow installation on a remote Ubuntu server. We used the scheduler to execute the Airflow DAGs, a simple username & password auth scheme, and local logfiles for storing information about the status of Airflow's DAGs.

For a more production-ready system, you'd likely want to configure the following.

- **HTTPS only connections**: Since Airflow will store extremely sensitive information, like database passwords, you should only allow HTTPS connections. You'll need to run airflow on a subdomain of a purchased domain and use a service like LetsEncrypt for enabling HTTPS.
- **Remote logging**: Our setup uses the `~/airflow/logs/...` directory for storing the logfiles from task runs. If the server ever becomes inaccessible, those logs will be lost. A better configuration would use Airflow's built in logging to S3.

Thank you for following along! If you run into any issues, please get in touch using the "Contact" button.
