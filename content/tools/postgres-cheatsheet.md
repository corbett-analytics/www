---
list: false
---

Create a column

```sql
ALTER TABLE table_name ADD COLUMN new_column_name data_type
```

Update a column

```sql
UPDATE table
SET column1 = value1,
    column2 = value2 ,...
WHERE
   condition;
```

Drop a column.

```sql
alter table orders drop column description
```

Rename a column.

```sql
alter table orders rename column revenues to amount;
```

Create an index

```sql
CREATE INDEX idx_address_phone
ON address(phone);
```

Drop an index

```sql
drop index idx_address_phone
```
