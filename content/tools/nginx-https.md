---
title: "Nginx"
description: Build a server with Nginx
date: 2019-06-30T12:00:00Z
list: false
displayInList: true
dropCap: false
---

<!--  TODO: add a preamble about the problem that Nginx solves and why it's so popular -->

In this tutorial, we'll set up Nginx to serve a simple web app from an EC2 server. We'll also route traffic from a subdomain to a second application that's running on the same server, and optionally we'll configure our server to only use HTTPS.

To follow along with this tutorial, you'll need shell access to an EC2 server that's running Ubuntu 18, and if you're interested in configuring HTTPS support for your application you'll also need to have purchased a custom domain name. The custom domain is necessary for the automated SSL certificates that we'll be using from LetsEncrypt.

## Install nginx

To get started, install nginx.

```
sudo apt update && sudo apt install nginx
```

Nginx will start automatically, and you can check that everything has been installed correctly by using `systemctl`.

```
systemctl status nginx
```

For this tutorial, I'll be using an EC2 server with a public IP address `xx`. You can check whether nginx is actually responding to web requests, but you will probably see that it's not able to respond: by default EC2 instances are not configured to be accessible to web traffic.

```
curl [my-ec2-instance-public-up]
curl: (7) Failed to connect to 34.218.139.64 port 80: Connection timed out
```

To fix this issue, we'll assign a _security group_ to our instance. Security groups function as rules on a firewall: they determine which IP addresses can reach the instance, as well as what kinds of traffic the instance can respond to.

## Security groups

..

## Run a web app

- Install pip3

```
sudo apt-get install python3-pip
```

Install Flask and Gunicorn.

```
pip3 install flask gunicorn
```

If you're not familiar with these modules, it's worth taking a minute to understand what each one does.

*Flask* is a web application framework. It makes it easier to build responses to web requests. Like most web frameworks, it associates specific URLs with functions that you write, and each function is responsible for building the response to each request.

*Gunicorn* is a web server gateway interface. In our use case, it will serve as an intermediary between Flask and Nginx. The benefits to using gunicorn over just the simple web server that comes with Flask is that it's more reliable and faster. Although it's not strictly necessary to use Gunicorn, include it because you should never use the Flask web server for a production app.

After you've installed these modules with `pip3` you'll likely find that they're not actually available.

```
flask
bash: command not found: flask
```

That's because `pip3` installs the Python executables at `/home/ubuntu/.local/bin`, which isn't available on our `$PATH` variable.

Update your `$PATH` to include the install location for the Python executables.

```
export PATH=$PATH:/home/ubuntu/.local/bin
```

You should now be able to run the flask command.

```
flask --version
Python 3.6.8
Flask 1.1.1
Werkzeug 0.15.5
```

We'll create a simple web app using Flask.

```python
# /home/ubuntu/app.py

from flask import Flask, jsonify

app = Flask(__name__)

@app.route('/length/<string>')
def index(string):
    response = {
        'string': string,
        'length': len(string)
    }
    return jsonify(response)

if __name__ == "__main__":
    app.run()
```

Run the webserver in the background with the `--daemon` flag. This allows the webserver to stay running even when you log off the remote server.

```
gunicorn app:app --daemon --bind 127.0.0.1:8000
```

It's also important to note that `--bind` value that we've supplied. In our case, we'll telling Gunicorn to respond to requests on the localhost's network from port 8000. This means the webserver can't actually respond to requests from the internet yet, so going to `ec2-url:8000` won't work. Instead, we'll configure nginx to forward requests that it receives from port `80` to port `8000`.

## Configure Nginx

```
vi /etc/nginx/sites-enabled/flask
```

Add the following configuration to the file.

```
server {
	listen 80;
	server_name app.my-ec2-url.com;

	location / {
		proxy_set_header Host $host;
		proxy_pass http://127.0.0.1:8000;
		proxy_redirect off;
	}
}
```

Make sure that you replace the string after `server_name` with your EC2 instance's public URL.

In order for the configuration to become active, we need to restart nginx.

```
sudo systemctl restart nginx
```

Now retry the web connection.

```
curl http://xx/length/my-text-string
{
  "string": "my-text-string",
  "length": 14
}
```

That's it! You've configured Nginx to serve simple requests from a web application.

One of the limitations of our server is that it's not able to use HTTPS. To use configure HTTPS we need to get a SSL certificate. [What do the certificates do?]

## Install letsencrypt's certbot

- Download and use a free certificate from [certbot](https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx).

Add the repositories with the software to apt.

```
sudo apt-get install software-properties-common
sudo add-apt-repository universe
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
```

Install the certbot client for Nginx.

```
sudo apt-get install certbot python-certbot-nginx
```

Run the certbot client to automatically download a certificate and update your nginx configuration to serve requests over HTTPs. You'll be prompted for some information about yourself.

```
sudo certbot --nginx
```
