---
title: "Analytics Architecture"
date: 2019-07-22T15:52:51Z
list: false
displayInList: false
---

We should try to create a visualization (just in slides is fine).

Key components:

Snowplow for event tracking
Airflow for ETL management
DBT for modeling & analytics
Metabase
Google sheets & google slides

## This is the first headline

Some code

```python
print('Hello world')
```

## A second headline

Last line
