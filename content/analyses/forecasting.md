---
title: "Forecasting"
date: 2019-07-22T21:32:06Z
list: false
displayInList: false
---

References:
- https://www.datasciencecentral.com/profiles/blogs/predicting-the-future

Some preamble text

## This is the first headline

Some code

```python
print('Hello world')
```

## A second headline

Last line
