---
# TODO:
# Add the prose
title: "Building cohort analyses"
date: 2019-06-30T12:00:00Z
list: true
frontpage: true
displayInList: false
dropCap: false
description: Tracking user cohorts shows the impact of product and marketing changes - and they're easy to set up
---

In this analysis we justify a recommendation for a company to adjust its Customer Acquisition Cost thresholds depending on the time of the year.

Most companies don't adjust their CAC based on the time of year, instead using a historical average.  

Cohort analyses can be performed on any _time series_ dataset, such as a table that logs customer orders or invoices.

For this analysis we're going to use an e-commerce orders dataset from the [UC Irvine machine learning repository](https://archive.ics.uci.edu/ml/datasets/online+retail).

Based on that data, we can justify a very counter-intuitive recommendation: the company should increase its CAC thresholds during its slowest time of the year, while decreasing its CAC thresholds during its busiest time.

Most companies that use CAC targets for their marketing budgets do not adjust them based on the time of year, so the recommendation is likely to be a new change, and would have a significant impact on the efficiency of their customer acquisition spending

## Set up the Postgres server

We'll be using Postgres, which is easy to install on MacOSX.

```
brew install postgres
```

You can check that the Postgres app is now running with `brew services`.

```
brew services list
```

Create a new Postgres database and user if you're installing Postgres for the first time. It's important to name both the user and database `$USER` because it will simplify the commands needed to run the rest of the SQL in this tutorial.

```
createdb $USER
createuser $USER
```

## Download example datasets

I've modified the table to make it easier to load the data into Postgres and improve the performance of some of the queries we'll run.

```
git clone https://gitlab.com/corbett-analytics/cohort-analysis.git &&
cd cohort-analysis
```

Load the dataset into your Postgres database.

```
gunzip < sql/create_orders.sql.gz | psql
```

You'll see some logging output as the command inserts data into your database. It might take a minute to finish depending on the computer you're using.

Once the table is finished loading, take a look at the first records from the `orders` table.

```bash
psql -c 'select * from orders limit 2;'
```

The table is likely very similar to an "orders" table that any company would have. Notice that the records here represent the individual items in an invoice, so the table name "orders" is slightly misleading.

{{% table "invoice_no,stock_code,invoice_date,customer_id,invoice_week,amount" "545314,POST,2011-03-01 14:11:00,12866,2011-02-28,60" "545324,48138,2011-03-01 14:58:00,13110,2011-02-28,8" %}}

The main differences between the dataset here and the one from the UCI repository are ...

## Creating the cohorts

Create the cohorts.

```sql
-- sql/create_cohorts.sql
create table cohorts as (
  select
    customer_id,
    invoice_week,
    sum(amount) as revenues,
    -- The first week the customer placed an order is it's cohort
    min(invoice_week) over (partition by customer_id) as cohort_week
  from orders
  group by 1, 2
  order by 1, 2
);
```

Explain the logic behind why this query makes sense

The most important piece of logic in this tutorial is the window function we've used to calculate a user's cohort.

Calculate weekly revenues.

```sql
-- sql/revenue_per_week.sql
select
  invoice_week,
  sum(revenues) as revenues
from cohorts
group by 1
order by 1 asc
```

{{% image "analyses/weekly_revenues.svg" %}}

Calculate 60 day payback from cohort.

```sql
-- sql/average_60_day_revenue_per_cohort.sql
select
  cohort_week,
  sum(revenues) / count(distinct(customer_id)) as arpu_60_day
from cohorts
-- Only include revenue from weeks within ~60 days
where invoice_week - cohort_week <= 60
group by 1
```

We can see that 60 day payback amounts are also strongly affected by seasonality. But unlike revenues which are high towards the end of the year, ARPU is lower, and when revenues are lower at the start of the year, ARPU is higher.

{{% image "analyses/weekly_revenue_vs_cohort_60_day_arpu.svg" %}}

The conclusion becomes even more apparent if we look at the cumulative revenue per cohort over time.

{{% image "analyses/cohort_arpu_by_age_in_weeks.svg" %}}

## Data caveats

Some caveats worth noting:

1. Our dataset only starts in December 2010. We're treating customers there as part of the "new" cohorts even though they may be long time, large customers that are actually returning. So including their orders in the CAC calculation may be inappropriate.
2. Assumes a 0-day lead time from marketing spend to customer acquisition
3. Yearly purchasing cycles could be important
