---
# This can be resources like:
# - the Fishtown analytics roundup
# - youtube video channels
# - Patrick McKenzie
# - Business of software talk
# - Reforce growth series
# - Andrew Chen's blog

title: "The best blogs for tech analytics"  # This is the text for the link on Google. It will appear as "My title | Corbett Analytics"
h1: ""  # This appears first in the body text of the SERP link
description_tag: "" # This is sometimes (rarely) displayed in the SERP. Keep it less than 150 characters
background_image: ""  # TODO: create a background image for desktop browsers. Maybe one day it will be a video instead!
date: 2019-07-25T22:21:53Z
list: true
published: true
description: "A list of our favorite analytics related blogs"
---

Over the last few years we've been following several writers to learn more about industry trends, events, and related news. Finding good writing on analytics is hard (and part of the reason we started Corbett Analytics), so we wanted to share our list to help others that are interested in learning more about the industry.

We also thought it would be helpful to cover a broader range of topics than just analytics, since understanding the context (and jargon!) around business growth is critical to creating an effective analytics organization. Some of the topics we chose to include are more about software startups, finance, economics, and growth marketing, but all of them are intended to be helpful in increasing your impact.

Over time this list will likely change as we find new writers or current writers stop publishing. If you'd like to recommend a blog feel free to contact us and let us know.

So, in no particular order...

<!--  
0. Who is the author and what are their "credentials" / career
1. What topics do they cover?
2. Why are those topics relevant for an analytic's professional?
3. What are some illustrative examples of their best work?
-->

<!--  

What are the kinds of readings that benefit analytics practitioners?
  -> SaaS context: what are owners actually thinking about? What are the problems they're trying to solve? Example: why do successful companies lose so much money?
  -> Tools: what new types of tools are people developing, using, how to evaluate them?
  -> Analytics: how are company's using data to affect their decision making?

-->

## [Patrick Mckenzie's blog](https://www.kalzumeus.com/archive/)


Patrick is currently an employee at Stripe, where he works on their service Atlas, which helps new founders start their technology businesses. He's also a developer and multiple time founder, creating companies like bingocardcreator.com and a consulting company Kalzumeus Softare. He's highly active on twitter as [Patio11](https://twitter.com/patio11) if you're into that.

He writes mainly about start ups, growth, career advice, and also shares compelling personal anecdotes. His articles are also exceptionally well written, and he's precise in what he says even on broad topics.

We think that understanding the context around growth, especially for earlier stage companies, is what makes analytics professionals achieve their biggest impact. Patrick is maybe the best at illustrating that context that we've been able to find.

Some of our favorite articles:

- Why most technical people are afraid of [salary negotion](https://www.kalzumeus.com/2012/01/23/salary-negotiation/) and what you can do about it
- Perspective on [the Japanese earthquake in 2011](https://www.kalzumeus.com/2011/03/13/some-perspective-on-the-japan-earthquake/), also featured in the NYTimes

## [Stripe Atlas guides](https://stripe.com/atlas/guides/)

Speaking of Patrick, his recent work for Stripe Atlas is especially note worthy. It's not published very frequently, but it's definitely worth a bookmark and checking in on every so often.

The guides cover mostly the business of saas, with a focus on issues relevant to new or young companies. They also include AMAs with profiles like John Doerr and Mark Andreeson. They don't explicitly cover analytics per-se, but the context about SaaS businesses that you'll learn will likely help drive quality of your analysis.

Some examples of their best work:

-  [How to price a SaaS product](https://stripe.com/atlas/guides/saas-pricing)
-  [The business of saas](https://stripe.com/atlas/guides/business-of-saas)

## [Tristan Handy's analytics roundup](http://roundup.fishtownanalytics.com/)

Tristan is the founder of [Fishtown Analytics](https://www.fishtownanalytics.com/), an analytics consulting company that also maintains [DBT](https://docs.getdbt.com/), a data warehouse "modeling" tool.

He mostly covers news around data engineering, and has great insights especially about how organizations should approach their analytics processes. You can get updates on [his Twitter profile](https://twitter.com/jthandy) as well.

In addition to the roundup, he also writes on the Fishtown analytics blog.

Some of our favorites include:

- [Three years in for DBT](https://blog.fishtownanalytics.com/three-years-in-things-are-heating-up-2a72110d161d). It's a product with a vision for a new workflow and its working
- [Does your data team need a data engineer?](https://blog.fishtownanalytics.com/does-my-startup-data-team-need-a-data-engineer-b6f4d68d7da9) The short answer is probably no.

## [Andrew Chen's newsletter](https://andrewchen.co/)

Andrew Chen is maybe the most popular writer in the Bay Area about growth and related topics.  He currently works for Andreesen Horowitz, publishing posts [on their blog](https://a16z.com/), and he's also pretty active on Twitter.

While he doesn't discuss topics specific to data engineering, he often talks about his experience as a consumer of analytics. As an investor, it's also helpful to learn about how they weigh the value of various software metrics. Your analytics will be more impactful with a stronger understanding of what ends up mattering and what doesn't, from the perspective of an owner.

One caveat: recent stuff seems like it is getting written by other people and just edited by Andrew. That's mostly forgivable but I personally have felt the earlier stuff was more engaging for me.

Some of the recent highlights include:

- [Building a growth team](https://andrewchen.co/how-to-build-a-growth-team/)
- [The power user curve](https://andrewchen.co/power-user-curve/)

## [Brian Balfour's weekly emails](https://brianbalfour.com/)

Brian is another exceptionally popular writer around growth and analytics. He's also the founder of Reforge, a paid course for growth practitioners that I'd recommend if you get the opportunity to spend a self-improvement budget.

If there's one thing you can take away from Brian it's the critical impact of retention as the most important metric for businesses with a recurring revenue model. It seems intuitive to me now how retention is the foundation of growth, yet many companies don't treat retention as a growth metric - it's considered an issue addressed with customer support or a "tech debt" budget.

The top essays from Brian include:

- [The one growth metric](https://www.reforge.com/blog/growth-metric-acquisition-monetization-virality), ie retention
- [Running a growth meeting](https://brianbalfour.com/essays/growth-meeting)

## [Casey Winter's blog](https://caseyaccidental.com/)

Casey is the chief product officer at Eventbrite, and he's a "growth adviser in residence" at Greylock Partners, a VC firm. Previously he worked at Pinterest, Grubhub, and apartments.com. He also creates content for the Reforge growth series mentioned above.

Most of his writing is about startups, growth, and careers. Like Brian Balfour and Andrew Chen, he blurs the line between analytics and other roles, but the insights and lessons in his writing are certainly applicable to analytics teams as well.

Examples:

- [The analyst career path](https://caseyaccidental.com/the-analyst-career-path/)
- [The autonomy spectrum](https://caseyaccidental.com/the-autonomy-spectrum)

## [Howard Mark's memos](https://www.oaktreecapital.com/insights/howard-marks-memos)

Howard is a longtime popular speaker and writer, often appearing on TV shows like CNBC. His memos date back to the early 90s, and focus more on macro-economics and finance than software specifically.

Towards Data Science (TDS) is a popular medium-based blog. They source content from hundreds of writers, usually data scientists working at technology companies.

## [Joel on Software](https://www.joelonsoftware.com/)

Last but maybe our favorite, Joel is a long time blogger about Software companies, starting around 2001. He's a founder of Stack Overflow, and writes mostly humorous but relevant topics for software company founders.

I can't say that the content will _immediately_ impact your career. It's more for if you're feeling low energy and just want to enjoy something relevant, but that's fun and easy to read.

Some of the classics:

[The iceburg secret revealed (2002, a classic)](https://www.joelonsoftware.com/2002/02/13/the-iceberg-secret-revealed/)
[Fixing venture capital (2003)](https://www.joelonsoftware.com/2003/06/03/fixing-venture-capital/)
[Strange and maddening rules](https://www.joelonsoftware.com/2018/04/23/strange-and-maddening-rules/)

Finally, there's a few others blogs, podcasts, and sites I'd recommend. We'd love to hear from you if you've got other favorites we haven't included.

[Noah Kagan's podcast blog](https://okdork.com/blog/)
[Draft.nu blog](https://draft.nu/blog/)
[Locally Optimistic](https://www.locallyoptimistic.com/)
[Mark's Blogg](https://tech.marksblogg.com/is-hadoop-dead.html)
[Towards Data Science](https://towardsdatascience.com/)
[Data Science Central](https://www.datasciencecentral.com/)
