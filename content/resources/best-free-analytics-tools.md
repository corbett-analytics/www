---
title_tag: "Best Free Analytics Tools"  # This is the text for the link on Google. It will appear as "My title | Corbett Analytics"
h1: ""  # This appears first in the body text of the SERP link
description_tag: "" # This is sometimes (rarely) displayed in the SERP. Keep it less than 150 characters
background_image: ""  # TODO: create a background image for desktop browsers. Maybe one day it will be a video instead!
date: 2019-07-31T02:57:08Z
list: false
displayInList: false
---

<!-- Any preamble text will appear on the SERP link body. Make sure it's relevant for that reader as well -->

1. Preamble:
- What is the value of this content to the reader? State how they will benefit from learning the skills.
- What's the key insight they'll see in the example?

2. Dbt project
- Find a dataset to use
- Do some exploratory analysis

3. Create an outline with the major questions
- Write the sql components
- Iterate the outline
- Build the visual components
- Finish the prose
- Publish

## WIP

Airflow
Snowplow
Postgres with cstore extension
DBT
Gitlab
Metabase
Google Drive
