---
title: "The role of analytics: insight engineering"
date: 2019-06-30T12:00:00Z
list: true
frontpage: true
displayInList: true
dropCap: false
description: Companies today rely on analytics orgs to be both proficient engineers and business analysts. Learn how to manage both
---

Analytics organizations today typically have two types of deliverables: data products and insights.

Data products are really just software products, and the ability to produce high-quality outcomes in those projects is determined more by software development practices than anything else.

Successfully producing high-quality insights, however, is far more determined by the ability to bring in relevant context and speak to that context in a compelling way. The quality of software used or developed in that process is really just a secondary concern.

Given that there are different skills required to be successful at producing each type of output, it seems strange for organizations to lean on a single team to build both of them.

But when teams can do both effectively, the results are drastically improved relative to separating the concerns into different functions.
