---
title: How analytics teams scale
date: 2019-06-30T12:00:00Z
list: true
frontpage: true
displayInList: true
dropCap: false
description: Analytics organizations are significantly different at larger companies than smaller ones. Understand the reasons why
---

Every effective analytics organization views their software skills as a core competency. Especially since the release of Redshift in 2013, the first commercial columnar store database, the BI stack has decreased in cost while new free tools have improved in quality.

The choice between buying analytics solutions and building your own should follow the same cost/benefit analysis that you would apply for any "buy or build" assessment. Companies generally progress from "buy" to "build" in the following stages.

### Small to medium business [5-100 employees]

Most companies at this size choose to outsource their analytics stack almost completely. They'll typically implement a solution like segment, heap, or amplitude. All of those vendors supply a "full stack" solution: client libraries for logging events, a fully managed event collection web service, data pipelines for third party services like Salesforce, Mailchimp, etc, and analysis tools with SQL and/or GUI interfaces.

It is rarely cost effective for companies at this size to "insource" much of their analytics stack. The exceptions are companies that are generally older and started before the cheap and highly available of the analytics solutions, or companies with products that are highly dependent on analytics for systems like recommendations or lead targeting.

At an earlier stage or a smaller size of a company, we almost always recommend taking an "off the shelf" solution rather than building event collection apps or managing pipelines yourself. There are many free, easy to implement query interfaces too: metabase is perhaps the best in class at the time this was written. For a python based environment, it's easy to either run your own jupyter notebook hubs or cheap to use a tool like Mode.

### Medium to large businesses [100-1000 employees]

At this stage companies are starting to spend nearly a FTE worth of budget on their analytics stack, with the largest cost generally being their data warehouse. They've also usually hired at least a few analysts that support team leads in a strategic capacity: most commonly to support their marketing organizations, but occasionally also to support the product managers, sales operations, or customer support operations.

Companies will generally want more control over their data warehouse than the vendors with full-stack solutions are able to provide: the vendors need to keep their solutions working for all of their customers, and specific adaptations are not cost effective for them to support. So companies generally start to invest in analytics outside of those solutions, and they will either hire data engineers or repurpose some of their existing engineers accordingly.

We recommend that companies start to manage their data warehouse themselves, with the most common choices being Redshift, Bigquery, or Snowflake. The event collection services all offer easy to configure pipelines to write data into your data warehouse, while vendors like Stitch make it easy to send 3rd party data from a CRM, Google Analytics, or other marketing tools as well.

### Enterprises [1000+ employees]

Once at this stage, most companies become a sort of wild west in developing their analytics. Budgets are large enough to support building internal solutions, and most organizations at the company will have at least a small team of analysts to use those products, plus a few internal tool product managers to direct their development.

Companies at this phase start to develop large data science and data engineering teams to drive new products, such as ML-based recommendation systems and realtime streaming applications. Analytics organizations need to transition from being focused primarily on analysis to being primarily focused on building software products used by other teams, although supporting analysis is still important.

### Big tech [10000+ employees]

The challenges facing the analytics functions at these organizations are markedly different from the challenges facing smaller companies. Their budgets are large enough, and they've been around long enough, that nearly every piece of their stack is custom built and highly integrated into the other systems at the company. The success of their analytics depends primarily on the reliability, security, and training the users of their systems.

We generally see companies start to open source their internal tools at this point to more cost effectively facilitate their continued development and maintenance. Success stories for tools from companies following that path include Airflow from Airbnb and Presto from Facebook.
