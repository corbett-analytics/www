---
title: "Working Models for Analytics Teams"
date: 2019-07-22T21:32:53Z
list: false
displayInList: false
---


References:
- https://medium.com/@djpardis/models-for-integrating-data-science-teams-within-organizations-7c5afa032ebd

Some preamble text

## This is the first headline

Some code

```python
print('Hello world')
```

## A second headline

Last line
