function shiftWindow() {
  console.log("shifted")
  scrollBy(0, -100)
};

function createTableOfContents() {
  toc = $(".toc");

  if (toc.length == 0) {
    return
  }

  sections = $(".article").find('h2')

  for (section of sections) {
    toc.append('<a class="nav-link" href='+ '#' + section.id + '>' + section.innerText + '</a>')
  }
}

$(document).ready(function() {
  if (location.hash) {
    console.log("found hash: " + `${location.hash}`)
    shiftWindow()
  };

  createTableOfContents();

  window.addEventListener("hashchange", shiftWindow);
})
