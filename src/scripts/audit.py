import os
from pathlib import Path
from frontmatter import load
from click import secho

# TODO: make sure there's an img for each article and list section
def check_image(pfile):
    # Check the image is defined
    if 'image' not in pfile.keys():
        return False
    # Check the image actually exists
    else:
        image_name = pfile['image']
        image_path = os.path.join(f'/app/src/images/{pfile["image"]}')
        if not os.path.exists(image_path):
            return False
    return True


def check_title(pfile):
    return 'title' in pfile.keys()

def check_title_length(pfile):
    title = pfile.get('title')
    if title is None:
        title_length = 0
    else:
        title_length = len(title)
    return 30 <= title_length <= 50

def check_content_length(pfile):
    return 1000 < len(pfile.content.split()) < 1500

def check_h1(pfile):
    return 'h1' in pfile.keys()

def check_keywords(pfile):
    return 'keywords' in pfile.keys()

def check_keywords_length(pfile):
    return 'keywords' in pfile.keys() and type(pfile['keywords']) == list and len(pfile['keywords']) > 0

def audit(requirements, content_dir='/app/content'):
    for file in Path(content_dir).glob('**/*.md'):
        result = {}
        pfile = load(file)
        result['file'] = str(file)
        for req, fn in requirements.items():
            req_name = req.replace('check_', '')
            result[req_name] = fn(pfile)
        yield result

if __name__ == "__main__":
    _vars = vars().copy()
    requirements = {function: _vars[function] for function in _vars if 'check_' in function}
    for res in audit(requirements):
        secho(res.pop('file'))
        for key, val in res.items():
            fg = 'green' if val else 'white'
            secho(f'\t {key}: {val}', fg=fg)
