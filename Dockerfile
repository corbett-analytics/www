FROM registry.gitlab.com/pages/hugo:latest

RUN apk update
RUN apk add npm bash python3

# Copy contents
RUN mkdir /app
WORKDIR /app
COPY . /app

# Python installs
RUN pip3 install requests python-frontmatter click

# NPM install deps
RUN npm install

# Builds the static site
CMD ["hugo"]
